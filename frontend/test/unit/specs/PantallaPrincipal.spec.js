import Vue from 'vue'
import App from '@/components/PantallaPrincipal'

describe('PantallaPrincipal.vue', () => {
  it('should render correct contents', () => {
    const Constructor = Vue.extend(App)
    const vm = new Constructor({
      data: {
        msg: 'Welcome to Your Vue.js App',
        plantillas: [
          { nombre: 'Plantilla 1', estado: 'En Carga', descripcion: 'La primer plantilla' },
          { nombre: 'Plantilla 2', estado: 'Completa', descripcion: 'La segunda plantilla' },
          { nombre: 'Plantilla 3', estado: 'En Carga', descripcion: 'La tercer plantilla' },
          { nombre: 'Plantilla 4', estado: 'En Carga', descripcion: 'La cuarta plantilla' },
          { nombre: 'Plantilla 5', estado: 'Completa', descripcion: 'La quinta plantilla' },
          { nombre: 'Plantilla 6', estado: 'En Carga', descripcion: 'La sexta plantilla' },
          { nombre: 'Plantilla 7', estado: 'Completa', descripcion: 'La septima plantilla' },
          { nombre: 'Plantilla 8', estado: 'Completa', descripcion: 'La octava plantilla' }
        ]
      }
    }).$mount()
    expect(vm.$el.querySelector('a.navbar-item').textContent)
    .toEqual('DEJ')
  })
})
