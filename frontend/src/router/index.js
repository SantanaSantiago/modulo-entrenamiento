import Vue from 'vue'
import Router from 'vue-router'
import PantallaPrincipal from '@/components/PantallaPrincipal'
import CrearPlantilla from '@/components/CrearPlantilla'
import Preguntas from '@/components/Preguntas'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'PantallaPrincipal',
      component: PantallaPrincipal
    },
    {
      path: '/crear-plantilla',
      name: 'CrearPlantilla',
      component: CrearPlantilla
    },
    {
      path: '/preguntas/:plantilla',
      name: 'Preguntas',
      component: Preguntas
    }
  ]
})
