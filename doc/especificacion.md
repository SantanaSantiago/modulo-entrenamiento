# Módulo de Datos de Entrenamiento

## Acceso

* Se provee de una página para registro de docentes. En la página de registro se encuentra un formulario con los siguientes campos:
    - nombre completo
    - institución en la que trabaja
    - dirección de correo
    - materia

* Los registros ha de validarlos una persona (es importante verificar que los registros pertenezcan a docentes reales y que los campos estén bien cargados). Una vez se valida un registro le es enviado al docente un link con el que completar el registro (nombre de usuario y contraseña)

* Los docentes pueden loguearse usando su dirección de correo o usuario y su contraseña

## Pantalla Principal

* Una vez un docente se loguea en el sistema se le muestra la pantalla principal
![Imagen Principal][imagen-principal]

[imagen-principal]: imgs/pantalla-principal.png